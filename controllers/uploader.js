"use strict";

module.exports = (app) => {
  app.get("/", (req, res) => res.render("uploader"));

  app.post("/uploader", function (req, res, next) {
    upload(req, res, function (err) {
      if (err) {
        return res.end("Something went wrong!");
      }
      return res.end("File uploaded sucessfully!.");
    });
  });
};

async function create_record_file(filedata) {
  try {
    await File.create({
      filename: filedata.originalname,
      size: filedata.size,
      link: "qwerty",
    });
  } catch (err) {
    for (const { type } of err.errors || []) {
    }
    next(err);
  }
}
