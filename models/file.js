'use strict';

module.exports = (sequelize, Sequelize) => {
    const model = sequelize.define(
        'File',
        {
            filename: Sequelize.STRING,
            size: Sequelize.DOUBLE,
            link: Sequelize.STRING,
            upload_date: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
        },
        {}
    );

};
