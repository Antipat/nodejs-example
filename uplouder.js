module.exports = {
  upload_image: (filedata, res) => {
    if (!filedata) res.send("Ошибка при загрузке файла");
    else {
      if(!check_size_file(filedata.size)){
      res.send("Файл превышает лимит в 5Mb");

      }
      res.send("Файл загружен");
    }
  },

  check_size_file: (size) => {
    if (size > 500000) return false;

    return true;
  },
};
