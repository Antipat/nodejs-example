  module.exports.route = app => {
    app.get('/', (req, res) => {
        if (!req.user) return res.redirect('/login');
        res.render('index');
    });

    app.get('/hello', (req, res) => {
        res.render('hello');
    });

    app.get('/upload', (req, res) => {
        res.render('list_upload');
    });

    app.get('/list_upload', (req, res) => {
        res.render('list_upload');
    });
    
    app.use((req, res) => res.render('404'));
    
    app.use((err, req, res, next) => {
        winston.error(err.message, {url: req.url, err});
        res.render('500', {err});
        next;
    });
};
